#ifndef CRYPTO_HPP_INCLUDED
#define CRYPTO_HPP_INCLUDED
#ifdef __cplusplus
extern "C"{
#endif
#include <sodium.h>
#include <string>
#ifdef __cplusplus
}
#endif
#define pkSize crypto_box_PUBLICKEYBYTES
#define skSize crypto_box_SECRETKEYBYTES

typedef struct {

	std::string pubKey;
	std::string priKey;
} keypair;

keypair helper_genkp(std::string pubkey, std::string prikey) {
	keypair kp;
	kp.pubKey = pubkey;
	kp.priKey = prikey;

	return kp;
}


class Crypto {
private:
	keypair myKp;
public:
	Crypto() {
		///Here is where we initialize libsodium

		if(sodium_init() == -1) {
			///There was an issue with libsodium, we need to tell the user, and if possible, use a different crypto lib

			std::cout << "Fatal error: Libsodium has failed to initialize!" << std::endl;
		}
	}
	keypair receiver_genKeypair() {
		unsigned char ephemeralPK[pkSize]; //stores the public key
		unsigned char ephemeralSK[skSize]; //stores the secret key (private key)

		//Now we want to generate the key pair, and store it in our arrays
		if(crypto_box_keypair(ephemeralPK, ephemeralSK) != 0) {
			std::cout << "Failed to generate ephemeral keypair" << std::endl;
			exit(-1);
		}

		this->myKp.pubKey = std::string((const char *)ephemeralPK);
		this->myKp.priKey = std::string((const char *)ephemeralSK);
		return myKp;

	}
	std::string extractPublicKey()
	{
		return this->myKp.pubKey;
	}
	std::string extractPrivKey()
	{
		return this->myKp.priKey;
	}
	//only a sender can send a message
	std::string numstationEncrypt(std::string message, std::string pubkey, bool verbose)
	{
		///local helper function to prep our encrypted message
		///It converts our ciphertext and our hash to an std:string and plops a ';' between the two
		auto finalize = [=](std::string cipher, std::string _hash, char* delim)
		{
			return std::string(cipher += std::string((const char*)delim)).append(_hash);
		};



		///This will store our ciphertext that we convert to a string later
		unsigned char ciphertext[crypto_box_SEALBYTES + message.length()];

		///This will store our hash of our PLAINTEXT message, NOT our ciphertext
		unsigned char hash[crypto_auth_hmacsha512_BYTES];

		///This is a simple check to see if the user of this app decided to encrypt a message with their own public key
		///If they're intending on doing this to store a message for themselves, that should not be an issue
		if(verbose && this->myKp.pubKey == pubkey)
		{
			//Produce warning message
		}

		///produce the sealed message
		crypto_box_seal(ciphertext, (const unsigned char*)message.c_str(), message.length(), (const unsigned char*)pubkey.c_str());

		///Produce a hash for the sealed message, we need this to make sure that no attackers modify this in transit
		crypto_generichash(hash, sizeof hash, (const unsigned char*)message.c_str(), message.length(), NULL, 0);

		///finalize by combining the hash of the message with the ciphertext, seperating it by a ';'
		return finalize(
						std::string((const char*)ciphertext),
						std::string((const char*)hash),
						";"
					   );

	}

	//this is meant for a receiver to unpack a message that they have constructed
	std::string numstationDecrypt(std::string receivedMsg) {

	}
};
#endif // CRYPTO_HPP_INCLUDED
