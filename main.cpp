#include <iostream>
#include <cstdlib>
#include <cstring>
#include <string> //c++'s strings
#include <fstream>
#include <sstream>
#include <algorithm>
#include <vector>

#include "crypto/crypto.hpp"

///READ ME: Note to self, initialize Crypto class with keyfile to limit need to parse keyfiles on each call to encrypt or decrypt, which takes up a lot of memory
///			Store this in a keypair instance!!!!


/**
	NOTE: Version Alpha 0.0.1

	0.0.2: Error handle
		   USe exceptions when things like stream.bad() won't work :)

	This contains basic encryption and decryption, planned features go as follows:
		TOR numstation server
		One-Time-Pad style encrypted messages
		Full command line or full file I/O (print message to the screen, or place it into an encrypted file)
		Lossless compression for messages
		Steganography to send images instead of text files

		more intuitive cli

		interactive mode
**/


/*
idMsg is designed to identify if the string in cmd is equal to the string in param
*/
bool idMsg(const char* cmd, const char *param)
{
	if(strcmp(cmd,param)==0)
		return true;
	return false;
}


void printHelp(char** argv_ref)
{
	std::cout << "Usage: " << argv_ref[0] << " [-e, -d] [plaintext.txt keyfile.key] | [encrypted.enc (public_keyFile.pkey | private_keyFile.skey)] -o FileName" << std::endl;


	std::cout << "-e: Encrypt a message with a keyfile" << std::endl;
	std::cout << "\tExample: ./numstation -e /Desktop/myMessage.txt /Desktop/keyfile.key -o MyFileName" << std::endl;
	std::cout << "\tOutput will be in /Desktop/MyFileName.key" << std::endl;
}
std::vector<std::string> fileToVector(char* fileLocation)
{
	std::string line;
	std::string varToPush;
	std::vector<std::string> ret;

	std::ifstream fileStream(fileLocation);
	if(fileStream.bad()) {
		throw "Error: Bad filename";
	}
	while(std::getline(fileStream, line)) {
		std::stringstream ss(line);
		ss >> varToPush;
		ret.push_back(varToPush);
	}
	fileStream.close();

	return ret;
}


std::string extractFileExtension(std::string filename) {
	std::string extension;

	///Find the position of '.' in the file name
	int pos = filename.find(".");
	///verify the legitimacy of the file name
	if(!(pos >= 1)){
		throw "Not a valid filename!!";
	}
	///Extract the filename
	for(unsigned int iter = pos; iter < filename.size(); iter++){
		extension += filename[iter];
	}
	return extension;
}
int main(int argc, char **argv)
{
			Crypto *NumberStation = new Crypto();
			std::cout<< " _   _                 ____  _        _   _                     "    << std::endl;
            std::cout<< "| \\ | |_   _ _ __ ___ / ___|| |_ __ _| |_(_) ___  _ __         "    << std::endl;
            std::cout<< "|  \\| | | | | '_ ` _ \\\\___ \\| __/ _` | __| |/ _ \\| '_  \\  "    << std::endl;
            std::cout<< "| |\\  | |_| | | | | | |___) | || (_| | |_| | (_) | | | |       "    << std::endl;
            std::cout<< "|_| \\_|\\__,_|_| |_| |_|____/ \\__\\__,_|\\__|_|\\___/|_| |_|  "    << std::endl;



            if(argc > 1)
			{
				if(idMsg("--help", argv[1]) || idMsg("/?", argv[1]) || idMsg("-h", argv[1]), idMsg("--h", argv[1]))
				{
					printHelp(argv);
				}

				if(idMsg("-e", argv[1]))
				{
					///The user is going to encrypt a message
					if(!extractFileExtension(argv[2]) == std::string(".txt") || (!(extractFileExtension(argv[3])) == std::string(".pkey"))) {
						printHelp(argv);
						return -1;
					}

                    try {
                    	std::vector<std::string> fileData = fileToVector(argv[2]);
                    	std::vector<std::string> keyfile  = fileToVector(argv[3]); //This is the keyfile,
                    } catch(const char* msg) {
                    	std::cout << "\033[1m\033[31m" << msg << std::endl;
                    	printHelp(argv);
                    	exit(-1);
                    }
				}

			} else
			{
            	printHelp(argv);
            }
            return 0;
}
